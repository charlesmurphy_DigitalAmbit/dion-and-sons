﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dion_and_Sons.TWS;

namespace Dion_and_Sons
{
    class Program
    {
        static void Main(string[] args)
        {

            
            bool success;
            string errorMessage;
            string date = "11/10/2017";
            string regionID = "R!";
            string areaId = "R1";
            string routeID = "TESTROUTE2";
            int internalSessionID = 205312; // Primary Key in Routing Sessions Table [TSDBA].[RS_SESSION]
            string recipientID = "TEST";
            int internalRouteID = 15198821; //Primary Key in Routing Sessions Table [TSDBA].[RS_ROUTE]
            string wirelessId = "device1";

            DateTime date1 = DateTime.Today;
            bool validDate = DateTime.TryParse(date, out date1);

            RouteIdentity test = new RouteIdentity { regionID = regionID, routeDate = date1, routeID = routeID };

            LoadRouteToDevice(test, wirelessId);

            //Retrieve Route By Identity// Working

            //Route routeRetrievedByIdentity = RetrieveRouteUsingIdentity(date, regionID, routeID, out success, out errorMessage);

            //if (success == false)
            //{
            //    Console.WriteLine(errorMessage);
            //}


            //Return Notifications for recipientID// Working

            //List<Notification> notifications = RetrieveNotifications(recipientID, out success, out errorMessage);

            //if (success == false)
            //{
            //    Console.WriteLine(errorMessage);
            //}

            //Update Delivery Details // Not Working

            UpdateTheDeliveryDetails(date, regionID, routeID, out success, out errorMessage);

            //if (success == false)
            //{
            //    Console.WriteLine(errorMessage);
            //}

            //Retrieve Routes by Criteria // Working

            
            List<Route> routesRetrievedByCriteria = RetrieveRoutesUsingCriteria(date, regionID, routeID, out success, out errorMessage);

            NotificationIdentity[] notificationToDelete = new NotificationIdentity[1];
            notificationToDelete[0] = new NotificationIdentity
            {
                identity = 26955
            };

            //if (success == false)
            //{
            //    Console.WriteLine(errorMessage);
            //}

            //Delelte Notification by ID// Working
            DeletedTheNotificaitons(notificationToDelete, out success, out errorMessage);

            //if (success == false)
            //{
            //    Console.WriteLine(errorMessage);
            //}

            //Retrieve Routing Route// Working
            // RoutingRoute routingRoute = RetrieveRoutingRouteByIdentity(regionID, internalRouteID, regionID, internalSessionID, out success, out errorMessage);
        }
       
        
        static public List<Notification> RetrieveNotifications(string recipientId, out bool success, out string errorMessage)
        {
            success = false;
            errorMessage = null;

            TransportationWebServiceClient _Client = new TransportationWebServiceClient();
            List<Notification> notifications = new List<Notification>();

            RecipientIdentity ident = new RecipientIdentity
            {
                recipientID = recipientId,
            };

            TimeZoneOptions tzOptions = new TimeZoneOptions
            {
                embeddedInTimestamp = false,
                optionType = TimeZoneOptionsType.tzoLocalTimeZone,
                timeZone = TimeZoneValue.tmzNone
            };

            string lockIdentityForRoute = null;
            NotificationLockIdentity lockIdent = new NotificationLockIdentity
            {
                lockID = lockIdentityForRoute
            };

            NotificationRetrieveOptions options = new NotificationRetrieveOptions
            {
                autoDelete = false,
                lockIdentity = lockIdent,
                maxRetrieve = 5
            };

            try
            {
                notifications = _Client.RetrieveNotificationsByRecipientIdentity(ident, tzOptions, options).ToList();
                success = true;
            }

            catch (ApplicationException ex)
            {

                errorMessage = ex.Message;
            }

            return notifications;
        }

        static public RoutingRoute RetrieveRoutingRouteByIdentity ( string regiondId, int internalRouteId, string regionId, int internalSessionId, out bool success, out string errorMessage)
        {
            success = false;
            errorMessage = null;
            TransportationWebServiceClient _Client = new TransportationWebServiceClient();
            RoutingRoute route = new RoutingRoute();

            RoutingRouteIdentity identity = new RoutingRouteIdentity
            {
                regionID = regiondId, //required
                internalRouteID= internalRouteId, //required
                internalSessionID = internalSessionId //required
            };

            TimeZoneOptions tzOptions = new TimeZoneOptions
            {
                embeddedInTimestamp = false,
                optionType = TimeZoneOptionsType.tzoLocalTimeZone,
                timeZone = TimeZoneValue.tmzNone
            };
            RoutingRouteInfoRetrieveOptions options = new RoutingRouteInfoRetrieveOptions
            {
                level = RoutingDetailLevel.rdlRoute,
                retrievePublished = true,
                //   timeZoneOptions = tzOptions
                //    applyRoadnetAnywhereSyncRules =,
                //    retrieveActive =,
                //    retrieveActivities =,
                //    retrieveBuilt =,
                //    retrieveEquipment =,

                //   
            };
           
           
            try
            {
              
                route = _Client.RetrieveRoutingRouteByIdentity(identity, options);
                success = true;
            }

            catch (ApplicationException ex)
            {

                errorMessage = ex.Message;
            }

            return route;
        }

        static public Route RetrieveRouteUsingIdentity(string sDate, string regionId, string routeId, out bool success, out string errorMessage)
        {
            success = false;
            errorMessage = null;

            DateTime date = DateTime.Today;
            bool validDate = DateTime.TryParse(sDate, out date);

            TransportationWebServiceClient _Client = new TransportationWebServiceClient();
            Route route = new Route();

            RouteIdentity identity = new RouteIdentity
            {
                regionID = regionId,//required
                routeDate = date,//required
                routeID = routeId//required
            };

            TimeZoneOptions tzOptions = new TimeZoneOptions
            {
                //embeddedInTimestamp = false,
                //optionType = TimeZoneOptionsType.tzoLocalTimeZone
                // timeZone = TimeZoneValue.tmzNone
            };



            RouteInfoRetrieveOptions options = new RouteInfoRetrieveOptions
            {
                //level = DetailLevel.dlOrder,
                //retrieveActivities = false,
                //retrieveContainers = false,
                //retrieveDriverAlerts = false,
                //retrieveFinancialInformation = false,
                //retrieveRolledUpQuantities = false,
                //retrieveSurveyAssignments = false,
                //timeZoneOptions = tzOptions
            };

            try
            {
                route = _Client.RetrieveRouteByIdentity(identity, options);

                if(route == null)
                {
                     errorMessage = "No routes found that match route identity";
                    success = false;
                }
                success = true;
            }

            catch (ApplicationException ex)
            {

                errorMessage = ex.Message;
            }

            return route;
        }

        static public List<Route> RetrieveRoutesUsingCriteria (string sDate, string regionId, string routeId, out bool success, out string errorMessage)
        {
            success = false;
            errorMessage = null;
            DateTime date = DateTime.Today;
            bool validDate = DateTime.TryParse(sDate, out date);

            TransportationWebServiceClient _Client = new TransportationWebServiceClient();
            List<Route> routes = new List<Route>();

            EmployeeIdentity employeeIdent = new EmployeeIdentity
            {
               // employeeID = "EmployeeId",
               // regionID = regionId
            };
           
            
            RouteCriteria criteria = new RouteCriteria
            {
                regionID = regionId, //required
                routeDate = date, 
                
                //completed = false,
                //completedSpecified = false,
                //driverIdentity = employeeIdent,
                //gpsUnitID = gpsUnitId,
                //group= group,
                //internalRouteID= intRouteId,
                //internalRouteIDSpecified=false,
                routeDateSpecified =true
            };

            TimeZoneOptions tzOptions = new TimeZoneOptions
            {
                embeddedInTimestamp = false,
                optionType = TimeZoneOptionsType.tzoLocalTimeZone,
                timeZone = TimeZoneValue.tmzNone
            };
            
            RouteInfoRetrieveOptions options = new RouteInfoRetrieveOptions
            {
                level = DetailLevel.dlLineItem,
                retrieveActivities = true,
                //retrieveContainers = false,
                //retrieveDriverAlerts = false,
                //retrieveFinancialInformation = false,
                //retrieveRolledUpQuantities = false,
                //retrieveSurveyAssignments = false,
                timeZoneOptions = tzOptions,
               
            };

            try
            {
                routes = _Client.RetrieveRoutesByCriteria(criteria, options).ToList();
                success = true;
            }

            catch (ApplicationException ex)
            {

                errorMessage = ex.Message;
            }

            return routes;
        }

        static public void  UpdateTheDeliveryDetails (string sDate, string regionId, string routeId, out bool success, out string errorMessage)
        {
            success = false;
            errorMessage = null;
            DateTime date = DateTime.Today;
            bool validDate = DateTime.TryParse(sDate, out date);

            TransportationWebServiceClient _Client = new TransportationWebServiceClient();

            RouteIdentity identity = new RouteIdentity
            {
                regionID = regionId, //required
                routeDate = date, //required
                routeID = routeId //required
            };

            Quantities exampleQuantity = new Quantities
            {
                size1 = 59,
                size2= 0,
                size3 = 0
                
            };

            Quantities zero = new Quantities
            {
                size1 = 0,
                size2 = 0,
                size3 = 0

            };
            ItemQuantities deliveryUpdateQuantities = new ItemQuantities
            {
                actual = exampleQuantity,
                actualPickup = zero,
                damaged = zero,
                loaded = zero,
                over = zero,
                planned = zero,
                plannedPickup = zero,
                shorted = zero

            };

            ReasonCodes deliveryReasonCodes = new ReasonCodes
            {
                damagedCode = null ,
                deliveryCode = null,
                loadedCode = null,
                overCode = null,
                pickupCode = null,
                shortCode = null

            };

            OrderIdentity orderIdentity = new OrderIdentity
            {
                orderNumber = "TESTORDER3",
                regionID = regionId,
                routeDate = date,
                routeID = routeId,
                
                
            };

            OrderDeliveryDetailItem[] deliverDetailItem = new OrderDeliveryDetailItem[1];
            deliverDetailItem[0] = new OrderDeliveryDetailItem
            {
               orderIdentity= orderIdentity,
               quantities= deliveryUpdateQuantities,
               orderStatusCode = null,
               reasonCodes = null,
               referenceNumber= null,
              
               
            };
            
            DeliveryDetailInfo info = new DeliveryDetailInfo
            {
                deliveryDetails = deliverDetailItem, // Required
              //  messageId = messageID,
                performedAt = PerformedAt.epaStop, // Required This affects what values can be Updated
                routeIdentity = identity, // Required
             //   source = RouteEventSource.resDispatch,
             //   wirelessId = wirelessID
            };

            

            try
            {
                _Client.UpdateDeliveryDetails(info);
                success = true;
                
               
            }

            catch (ApplicationException ex)
            {

                errorMessage = ex.Message;
            }

            
        }

        static public void DeletedTheNotificaitons (NotificationIdentity[] notificationToDelete, out bool success, out string errorMessage)
        {
            success = false;
            errorMessage = null;
          
            TransportationWebServiceClient _Client = new TransportationWebServiceClient();

            try
            {
                
                _Client.DeleteNotifications(notificationToDelete);
                success = true;
            }

            catch (ApplicationException ex)
            {

                errorMessage = ex.Message;
            }


        }

        static public void LoadRouteToDevice(RouteIdentity route, string wirelessId)
        {
            List<int> successFaultCodes = new List<int>
            {
                1003, // already loaded
                1004  // already started
            };
            
            TransportationWebServiceClient _Client = new TransportationWebServiceClient();

            try
            {
                _Client.RetrieveRouteForDevice(
                    route,
                    new RouteInfoRetrieveOptions
                    {
                        timeZoneOptions = new TimeZoneOptions
                        {
                            timeZone = TimeZoneValue.tmzCentralTimeUSCanada
                        }
                    },
                    new WirelessDeviceIdentity
                    {
                        regionID = route.regionID,
                        wirelessID = wirelessId,
                        wirelessProvider = WirelessProvider.wpCustom
                    }
                );
              
            }
            catch (ApplicationException ex)
            {

              
            }
        }
    }
}
